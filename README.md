# context-qr

## What it does
This add-on adds an item to the browser's context menu, which allows to try to recognise an image as qr-code. It is based on webextension API. After successful recognition copies content to clipboard and shows a notification.

It uses [NPM port of Lazarsoft’s qrcode reader](https://github.com/edi9999/jsqrcode). 
Uses qr-code-scan icon made by [Freepik](http://www.freepik.com/) from [www.flaticon.com](http://www.flaticon.com/).