/*
Create a listener waiting for background to send the message with decoded text to copy it
*/
browser.runtime.onMessage.addListener(function (message) {
    let field = document.createElement("textarea");
    document.body.appendChild(field);
    field.value = message;
    field.select();
    document.execCommand("copy");
    field.remove();
});