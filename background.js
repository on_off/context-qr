/*
Called when the item has been created, or when creation failed due to an error.
We'll just log success/failure here.
*/
function onCreated(n) {
  if (browser.runtime.lastError) {
    console.log(`Error: ${browser.runtime.lastError}`);
  } else {
    console.log("Item created successfully");
  }
}

/*
Called when the item has been removed.
We'll just log success here.
*/
function onRemoved() {
  console.log("Item removed successfully");
}

/*
Called when there was an error.
We'll just log the error here.
*/
function onError(error) {
  console.log(`Error: ${error}`);
}

/*
Create all the context menu items.
*/
browser.contextMenus.create({
  id: "read-qr",
  title: browser.i18n.getMessage("contextMenuItem"),
  contexts: ["image"]
}, onCreated);

/*
Set a callback for succesful qrcode reading.
*/
var qr = new QrCode();


qr.callback = function (data, err) {
        console.log(data);
        console.log(err);
        if (err){
          browser.notifications.create("qrd_notif", {
            type: "basic",
            message: browser.i18n.getMessage("qrNotificationErrContents"),
            title: browser.i18n.getMessage("qrNotificationErrHeader")
          });
          return;
        }

        browser.notifications.create("qrd_notif", {
          type: "basic",
          message: data,
          title: browser.i18n.getMessage("qrNotificationHeader")
        });

        browser.tabs.executeScript({
          file: "copy-agent.js"
        });

        var querying = browser.tabs.query({
          active: true,
          currentWindow: true
        });

        querying.then(function (tabs) {
          browser.tabs.sendMessage(tabs[0].id, data)
        });
      }

/*
The click event listener, where we perform the appropriate action given the
ID of the menu item that was clicked.
*/
browser.contextMenus.onClicked.addListener(function(info, tab) {
  if (info.menuItemId == "read-qr") {
    qr.decode(info.srcUrl);
    console.log(info.srcUrl);
  }
});
